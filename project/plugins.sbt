addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.4.1")
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.5.1")
addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.15")
addCompilerPlugin(
  "org.scalameta" % "semanticdb-scalac" % "4.3.14" cross CrossVersion.full
)
