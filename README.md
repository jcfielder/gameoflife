Game of Life
============

## What is this?

This is an implementation of [Conway's Game of Life ](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).
Currently this only runs one generation, however it could easily be extended to run them sequentially.


How to build and run the project:
```bash
sbt stage
target/universal/stage/bin/gameoflife
```

How to test the project:
```bash
sbt test
```

How to use the project:
- Start the project as above
- Enter the live points as so, with `end` terminating the list of input:
```
1,2
2,2
3,2
end
```
- The output of the next generation of live cells will be printed to the console
```
2,1
2,2
2,3
end
```