package com.gitlab.jcfielder.gameoflife

trait Reader {
  def getLine: String
}

class ConsoleLineReader extends Reader {
  override def getLine: String = scala.io.StdIn.readLine()
}
