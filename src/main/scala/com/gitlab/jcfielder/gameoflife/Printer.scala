package com.gitlab.jcfielder.gameoflife

trait Printer {

  def pointPrinter(points: Seq[(Int, Int)]): Unit = {
    print(seqPrinter(points.sorted)(x => x._1.toString + "," + x._2.toString))
  }

  def seqPrinter[A](s: Seq[A])(stringify: A => String): String =
    s match {
      case Seq() => "end"
      case _ if s.nonEmpty =>
        s.foldLeft("")((acc, next) => acc + (stringify(next) + "\n")) + "end"
    }
}
