package com.gitlab.jcfielder.gameoflife

import shapeless.syntax.std.tuple._

object GameOfLife {

  // Takes a Seq of alive points and produces the next generation
  // of living points
  def gameOfLife(points: Seq[(Int, Int)]): Seq[(Int, Int)] = {
    val board: Seq[Cell] = generateBoard(points)

    board
      .map(b => countLiveCells(b, board))
      .map(a => game(a))
      .filter(_.alive)
      .map(_.toPoint)
  }

  def game(cell: Cell): Cell = cell match {
    case c @ Cell(x, y, true, Some(alive)) if alive < 2                => c.copy(alive = false)
    case c @ Cell(x, y, true, Some(alive)) if alive == 2 || alive == 3 => c
    case c @ Cell(x, y, true, Some(alive)) if alive > 3                => c.copy(alive = false)
    case c @ Cell(x, y, false, Some(alive)) if alive == 3 =>
      c.copy(alive = true)
    case c @ Cell(_, _, false, _) => c
  }

  // Let's generate the Cells in the form of (x, y, alive)
  def generateBoard(points: Seq[(Int, Int)]): Seq[Cell] = {
    points.map(p => Cell(p :+ true)) ++
      points
        .flatMap(generateNeighbours)
        .distinct
        .filter(p => !points.contains(p)) // Don't set a point that we know is alive to dead
        .map(p => Cell(p :+ false))
  }

  def countLiveCells(cell: Cell, allCells: Seq[Cell]): Cell = {
    val neighbours = generateNeighbours(cell.toPoint)
    val candidates = allCells.filter(
      c => neighbours.contains(c.toPoint) && !(cell.toPoint == c.toPoint)
    )

    cell.copy(aliveNeighbours = Some(candidates.count(_.alive)))
  }

  def generateNeighbours(point: (Int, Int)): Seq[(Int, Int)] = {
    val (x, y) = (point._1, point._2)

    cartesianProduct(ranges(x), ranges(y))
  }

  def ranges(x: Int): Seq[Int] = x - 1 to x + 1

  def cartesianProduct[A, B](a: Seq[A], b: Seq[B]): Seq[(A, B)] = {
    for {
      as <- a
      bs <- b
    } yield (as, bs)
  }
}
