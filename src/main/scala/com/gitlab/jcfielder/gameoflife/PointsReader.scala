package com.gitlab.jcfielder.gameoflife

import scala.util.Try

trait PointsReader extends ConsoleLineReader {

  def readLivePoints: Seq[(Int, Int)] = {
    readLivePoints().distinct
  }

  def readLivePoints(read: Seq[(Int, Int)] = Seq()): Seq[(Int, Int)] = {
    getLine match {
      case "end"         => read
      case point: String => (mapPoint(point) ++ read) ++ readLivePoints()
    }
  }

  def mapPoint(point: String): Seq[(Int, Int)] = {
    Try(point.split(",").map(_.toInt))
      .map {
        case Array(x, y) => Seq((x, y))
      }
      .getOrElse(Seq())
  }
}
