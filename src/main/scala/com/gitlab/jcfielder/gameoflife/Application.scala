package com.gitlab.jcfielder.gameoflife

import com.gitlab.jcfielder.gameoflife.GameOfLife._

object Application extends PointsReader with Printer with App {
  val points = readLivePoints()

  pointPrinter(gameOfLife(points))
}