package com.gitlab.jcfielder.gameoflife

// Make aliveNeighbours optional as we don't have the value until we've got the whole board
case class Cell(x: Int, y: Int, alive: Boolean = false, aliveNeighbours: Option[Int] = None) {
  def toPoint: (Int, Int) = (this.x, this.y)
}

object Cell {
  def apply(a: (Int, Int)): Cell = new Cell(a._1, a._2)
  def apply(cell: (Int, Int, Boolean)) = new Cell(cell._1, cell._2, cell._3)
}
