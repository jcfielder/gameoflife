package com.gitlab.jcfielder.gameoflife

import java.io.{ByteArrayOutputStream, StringReader}
import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec

class LineReaderSpec extends AnyFlatSpec with Matchers {
  import ConsoleTestUtils._

  "LineReader" should "read in a line from the console" in {
    val reader = new ConsoleLineReader

    val in = new StringReader("test")
    val out = new ByteArrayOutputStream()

    testConsole(in, out) {
      reader.getLine should be("test")
    }
  }
}
