package com.gitlab.jcfielder.gameoflife

import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec

class GameOfLifeSpec extends AnyFlatSpec with Matchers {
  import GameOfLife._
  "GameOfLife.generateNeighbours" should "make correct neighbours" in {
    generateNeighbours((1, 1)) should contain theSameElementsAs Seq(
      (1, 1),
      (0, 1),
      (2, 1),
      (0, 0),
      (0, 2),
      (1, 0),
      (1, 2),
      (2, 0),
      (2, 2)
    )
  }

  it should "give neighbours at negative numbers" in {
    generateNeighbours((0, 0)) should contain theSameElementsAs Seq(
      (-1, -1),
      (-1, 0),
      (-1, 1),
      (0, -1),
      (0, 0),
      (0, 1),
      (1, -1),
      (1, 0),
      (1, 1)
    )
  }

  "GameOfLife.generateBoard" should "generate 8 points for 1 alive point" in {
    val (alive, dead) = generateBoard(Seq((3, 3))).partition(_.alive)
    alive.map(_.toPoint) should contain theSameElementsAs Seq((3, 3))

    dead.size should be(8)
  }

  "GameOfLife.generateBoard" should "generate 16 points for 2 alive points which are far away" in {
    val (alive, dead) =
      generateBoard(Seq((3, 3), (100, 100))).partition(_.alive)
    alive.map(_.toPoint) should contain theSameElementsAs Seq(
      (3, 3),
      (100, 100)
    )

    dead.size should be(16)
  }

  "GameOfLife.generateBoard" should "generate 12 points for 2 alive points which are diagonal" in {
    val (alive, dead) = generateBoard(Seq((3, 3), (4, 4))).partition(_.alive)
    alive.map(_.toPoint) should contain theSameElementsAs Seq((3, 3), (4, 4))

    dead.size should be(12)
  }

  "GameOfLife.generateBoard" should "generate correct board for 3 alive points which are in a column" in {
    val (_, dead) =
      generateBoard(Seq((2, 1), (2, 2), (2, 3))).partition(_.alive)

    val expect = Seq(
      (1, 0),
      (2, 0),
      (3, 0),
      (1, 1),
      (3, 1),
      (1, 2),
      (3, 2),
      (1, 3),
      (3, 3),
      (1, 4),
      (2, 4),
      (3, 4)
    )
    dead should contain theSameElementsAs expect.map(Cell(_))
  }

  "GameOfLife.countLiveCells" should "count neighbouring live cells correctly" in {
    val cell = Cell(1, 2, alive = true, None)
    val allCells = Seq(cell, Cell(2, 2, alive = true, None))

    countLiveCells(cell, allCells) should be(Cell(1, 2, alive = true, Some(1)))
  }

  it should "not count cells which are far away from the cell we are looking at" in {
    val cell = Cell(1, 1, alive = true, None)
    val allCells = Seq(cell, Cell(100, 100, alive = true, None))

    countLiveCells(cell, allCells) should be(Cell(1, 1, alive = true, Some(0)))
  }

  it should "count a more complex arrangement correctly" in {
    val cell = Cell(1, 2, alive = true, None)
    val allCells = Seq(
      cell,
      Cell(2, 2, alive = true, None),
      Cell(2, 1, alive = true, None),
      Cell(0, 2, alive = true, None)
    )

    countLiveCells(cell, allCells) should be(Cell(1, 2, alive = true, Some(3)))
  }

  "GameOfLife.gameOfLife" should "play the game correctly" in {
    val out = gameOfLife(Seq((1, 2), (2, 2), (3, 2)))

    out should contain theSameElementsAs Seq((2, 1), (2, 2), (2, 3))
  }
}
