package com.gitlab.jcfielder.gameoflife

import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec

class PointsReaderSpec extends AnyFlatSpec with Matchers {
  implicit val reader: PointsReader with FakeConsoleReader = new PointsReader
  with FakeConsoleReader
  import FakeConsoleReader._
  "Fake reader" should "read end and produce no points" in {
    sendToConsole("end")

    reader.readLivePoints should be(Seq())
  }

  it should "read one point" in {
    sendToConsoleChained(Seq("1,2", "end"))

    reader.readLivePoints should be(Seq((1, 2)))
  }

  it should "read many points" in {
    sendToConsoleChained(Seq("1,3", "3,2", "end"))

    reader.readLivePoints should be(Seq((1, 3), (3, 2)))
  }

  it should "remove duplicate points" in {
    sendToConsoleChained(Seq("1,1", "1,1", "end"))

    reader.readLivePoints should be(Seq((1, 1)))
  }

  it should "ignore points it cannot parse" in {
    sendToConsoleChained(Seq("1", "end"))

    reader.readLivePoints() should be(Seq())
  }

  it should "not fail on points with text" in {
    sendToConsoleChained(Seq("hello", "world", "1,1", "end"))

    reader.readLivePoints() should be(Seq((1, 1)))
  }
}
