package com.gitlab.jcfielder.gameoflife

trait FakeConsoleReader extends Reader {
  var next: () => String = () => ""

  var callback: () => Unit = () => ()

  override def getLine: String = {
    val out = next()
    callback()
    out
  }
}

object FakeConsoleReader extends FakeConsoleReader {

  // Use mutable state in your tests, hope everything is good!
  def sendToConsole(in: String)(implicit r: Reader with FakeConsoleReader): Unit =
    r.next = () => in

  // This one will respond with the sequence of strings over each subsequent call
  def sendToConsoleChained(ins: Seq[String])(implicit r: Reader with FakeConsoleReader): Unit =
    andThenChained(ins, 0)

  private[this] def andThenChained(ins: Seq[String], index: Int)(implicit r: Reader with FakeConsoleReader): Unit = {

    if (index > ins.length) throw new ArrayIndexOutOfBoundsException("Your test has been called too many times, " +
      "supply more lines or fix your code")

    sendToConsole(ins(index))

    // On the final element, we don't try to access the array
    r.callback = if (index == ins.length - 1) () => () else () => {
      andThenChained(ins, index + 1)
    }
  }
 }
