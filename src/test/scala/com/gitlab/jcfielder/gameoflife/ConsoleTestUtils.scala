package com.gitlab.jcfielder.gameoflife

import java.io.{ByteArrayOutputStream, StringReader}

object ConsoleTestUtils {
  def testConsole[T](in: StringReader, out: ByteArrayOutputStream)(test: => T): T = {
    Console.withOut(out) {
      Console.withIn(in) {
        test
      }
    }
  }
}
