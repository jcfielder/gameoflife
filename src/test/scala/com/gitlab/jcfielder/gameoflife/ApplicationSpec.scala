package com.gitlab.jcfielder.gameoflife

import java.io.{ByteArrayOutputStream, StringReader}
import org.scalatest.matchers.should.Matchers
import org.scalatest.flatspec.AnyFlatSpec
import ConsoleTestUtils._
import org.scalatest.BeforeAndAfterEach

import scala.collection.mutable

class ApplicationSpec
    extends AnyFlatSpec
    with Matchers
    with BeforeAndAfterEach {

  import Application._

  var in = new StringReader("")
  var out = new ByteArrayOutputStream()

  override def afterEach(): Unit = {
    out = new ByteArrayOutputStream()
  }

  "Application" should "play the game" in {
    setupInput(
      Seq(
        "1,2",
        "2,2",
        "3,2",
        "1000000001,1000000002",
        "1000000002,1000000002",
        "1000000003,1000000002",
        "end"
      )
    )

    callMain should be("""2,1
                         |2,2
                         |2,3
                         |1000000002,1000000001
                         |1000000002,1000000002
                         |1000000002,1000000003
                         |end""".stripMargin)
  }

  it should "not break on no input" in {
    setupInput(Seq("end"))

    callMain should be("end")
  }

  private def callMain: String = {
    testConsole(in, out) {
      main(Array())

      out.toString
    }
  }

  private def setupInput(ins: Seq[String]): Unit = {
    val input = new mutable.StringBuilder()
    ins.foreach(input ++= _ + "\n")

    in = new StringReader(input.result())
  }
}
