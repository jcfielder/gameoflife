import Dependencies._

ThisBuild / scalaVersion := "2.13.2"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.gitlab.jcfielder"
ThisBuild / semanticdbEnabled := true
ThisBuild / semanticdbVersion := "4.3.14"

lazy val root = (project in file("."))
  .settings(
    name := "gameoflife",
    libraryDependencies ++= Seq(shapeless, scalaTest % Test),
    addCompilerPlugin(scalafixSemanticdb),
    scalacOptions += "-Wunused:imports"
  )

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)

enablePlugins(JavaAppPackaging)

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
